﻿namespace EmployeeManagement.Employees.domain
{
    public interface IEmployeeRepository
    {
        public Task<Employee> Get(Guid id);
        public Task<IEnumerable<Employee>> GetAllSorted();
        public Task<List<Employee>> FindEmployeesReportingTo(Guid superiorId);
        public Task<List<Employee>> FindCEOs();
        public Task Add(Employee employee);
        public Task Update(Employee employee);
        public Task Delete(Guid id);
    }
}
