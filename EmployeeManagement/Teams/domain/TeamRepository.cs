﻿using EmployeeManagement.Config;
using Microsoft.EntityFrameworkCore;

namespace EmployeeManagement.Teams.domain
{
    public class TeamRepository : ITeamRepository
    {
        private readonly EmployeeManagementContext _context;

        public TeamRepository(EmployeeManagementContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Team>> GetAll()
        {
            return await _context.Teams.ToListAsync();
        }

        public async Task<Team> Get(Guid id)
        {
            return await _context.Teams.FindAsync(id);
        }

        public async Task Add(Team unit)
        {
            await _context.Teams.AddAsync(unit);
            await _context.SaveChangesAsync();
        }

        public async Task Update(Team unit)
        {
            _context.Entry(unit).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task Delete(Guid id)
        {
            var unit = await _context.Teams.FindAsync(id);
            _context.Teams.Remove(unit);
            await _context.SaveChangesAsync();
        }
    }

}
