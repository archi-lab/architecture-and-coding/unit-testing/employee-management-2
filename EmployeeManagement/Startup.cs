﻿using EmployeeManagement.Config;
using EmployeeManagement.Employees.application;
using EmployeeManagement.Employees.domain;
using EmployeeManagement.Teams.domain;

namespace EmployeeManagement
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddEndpointsApiExplorer();
            //services.AddSwaggerGen();

            services.AddScoped<IEmployeeRepository, EmployeeRepository>();
            services.AddScoped<IEmployeeService, EmployeeService>();
            services.AddScoped<ITeamRepository, TeamRepository>();

            // HttpClient
            services.AddHttpClient();

            // Register the EmployeeManagementContext with a specific database provider
            services.AddScoped<EmployeeManagementContext>();
            services.AddDbContext<EmployeeManagementContext>();
        }


        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                //app.UseSwagger();
                //app.UseSwaggerUI();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            // Ensure the database is created at startup
            using (var scope = app.ApplicationServices.CreateScope())
            {
                var context = scope.ServiceProvider.GetService<EmployeeManagementContext>();
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();

                // Seed the database with random data
                DataInitializer dataInitializer = new DataInitializer(context);
                dataInitializer.Seed();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();  // Serve static files from the wwwroot folder
            app.UseDefaultFiles(); // Serve the index.html file by default
            app.UseRouting();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapFallbackToFile("index.html");
            });

        }
    }
}
