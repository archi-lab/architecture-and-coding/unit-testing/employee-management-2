﻿using EmployeeManagement.Employees.domain;
using EmployeeManagement.Teams.domain;

namespace EmployeeManagement.Config
{


    public class DataInitializer
    {
        private EmployeeManagementContext _context;
        private static readonly string[] _EXTERNAL_NAMES = { "Leanne Graham", "Ervin Howell", "Clementine Bauch", "Patricia Lebsack", "Chelsey Dietrich", "Kurtis Weissnat", "Glenna Reichert" };


        public DataInitializer(EmployeeManagementContext context)
        {
            _context = context;
        }

        public void Seed()
        {
            /*
            if (!_context.Teams.Any())
            {
                var Teams = GenerateRandomTeams(20);
                _context.AddRange(Teams);
                _context.SaveChanges();
            }
            */

            if (!_context.Teams.Any() && !_context.Employees.Any())
            {
                var topManagers = GenerateRandomTopManagers(2);
                var middleManagers = GenerateRandomMiddleManagers(topManagers, 5);
                GenerateRandomEmployees(middleManagers, 5);
            }
        }

        private List<Team> GenerateRandomTeams(int count)
        {
            var Teams = new List<Team>();

            for (int i = 0; i < count; i++)
            {
                var Team = new Team
                {
                    Name = $"Abteilung {i + 1}",
                };

                Teams.Add(Team);
            }

            return Teams;
        }

        private List<Employee> GenerateRandomTopManagers(int count)
        {
            var topManagers = new List<Employee>();
            for (int i = 0; i < count; i++)
            {
                var employee = Employee.Random(null, null, null);
                topManagers.Add(employee);
            };
            _context.AddRange(topManagers);
            _context.SaveChanges();
            return topManagers;
        }


        private List<Employee> GenerateRandomMiddleManagers(List<Employee> topManagers, int count)
        {
            var middleManagers = new List<Employee>();
            for (int iTop = 0; iTop < topManagers.Count; iTop++)
            { 
                for (int iMiddle = 0; iMiddle < count; iMiddle++)
                {
                    var employee = Employee.Random(topManagers[iTop], Employee.MANAGER_ROLE, 'B');
                    middleManagers.Add(employee);
                }
            };
            _context.AddRange(middleManagers);
            _context.SaveChanges();
            return middleManagers;
        }


        private void GenerateRandomEmployees(List<Employee> managers, int count)
        {
            var employees = new List<Employee>();
            for (int iMgr = 0; iMgr < managers.Count; iMgr++)
            {
                for (int iEmp = 0; iEmp < count; iEmp++)
                {
                    var employee = Employee.Random(managers[iMgr], null, null);
                    employees.Add(employee);
                }
            };
            employees = ReplaceExternalNames(employees);
            _context.AddRange(employees);
            _context.SaveChanges();
        }


        private List<Employee> ReplaceExternalNames(List<Employee> employees)
        {
            for (int i = 0; i < _EXTERNAL_NAMES.Length; i++)
            {
                employees[i].Name = _EXTERNAL_NAMES[i];
                employees[i].Phone = null;
            }
            return employees;
        }
    }

}
